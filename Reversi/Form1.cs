﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reversi
{
    public partial class Form1 : Form
    {
        Board board;
        private CancellationTokenSource tokenSource;
        private BufferedGraphicsContext currentContext = BufferedGraphicsManager.Current;

        public Form1()
        {
            InitializeComponent();
            comboBox1.Items.Add("Heuristic 1");
            comboBox1.Items.Add("Heuristic 2");
            comboBox1.Items.Add("Heuristic 3");
            comboBox1.Items.Add("Heuristic 4");
            comboBox1.SelectedItem = comboBox1.Items[0];

            comboBox2.Items.Add("Heuristic 1");
            comboBox2.Items.Add("Heuristic 2");
            comboBox2.Items.Add("Heuristic 3");
            comboBox2.Items.Add("Heuristic 4");
            comboBox2.SelectedItem = comboBox2.Items[0];

            //Board test = new Board();
            //test.ChangeSquare(0, 7, 1);
            //test.ChangeSquare(0, 6, 1);
            //test.ChangeSquare(1, 7, 1);
            //test.ChangeSquare(2, 7, 1);
            //test.ChangeSquare(1, 6, 1);
            //test.ChangeSquare(0, 5, 1);
            //test.ChangeSquare(0, 4, 1);
            //test.ChangeSquare(1, 5, 1);
            //test.ChangeSquare(2, 6, 1);
            //test.ChangeSquare(3, 7, 1);
            //test.ChangeSquare(0, 3, 1);
            //test.ChangeSquare(1, 4, 1);
            //HeuristicUntakeablePiecesScore h = new HeuristicUntakeablePiecesScore();
            //int s = h.CalculateScore(test);
        }

        private void DrawBoardState(Board board)
        {
            BufferedGraphics buffer = currentContext.Allocate(pictureBox1.CreateGraphics(),
               pictureBox1.DisplayRectangle);

            Pen p = new Pen(Color.Black);
            SolidBrush sb = new SolidBrush(Color.Black);
            buffer.Graphics.Clear(pictureBox1.BackColor);
            int k = 0;
            while (k * 50 < 400)
            {
                buffer.Graphics.DrawLine(p, new Point(0, k * 50), new Point(pictureBox1.Width, k * 50));
                buffer.Graphics.DrawLine(p, new Point(k * 50, 0), new Point(k * 50, pictureBox1.Height));
                ++k;
            }

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (board.GetSquare(i, j) == 1)
                        buffer.Graphics.FillEllipse(sb, j * 50 + 5, i * 50 + 5, 40, 40);
                    if (board.GetSquare(i, j) == 2)
                        buffer.Graphics.DrawEllipse(p, j * 50 + 5, i * 50 + 5, 40, 40);
                }
            }
            buffer.Render();
        }

        private void UpdateScore(Board board, ref double b, ref double w)
        {
            if (board.GetBlack() > board.GetWhite()) ++b;
            else if (board.GetBlack() < board.GetWhite()) ++w;
            else
            {
                b += 0.5;
                w += 0.5;
            }
        }
       
        private async void newMatchButton_Click(object sender, EventArgs e)
        {
            try
            {
                tokenSource = new CancellationTokenSource();
                CancellationToken token = tokenSource.Token;
                await NewMatch(token);
            }
            catch (TaskCanceledException)
            {

            }
        }

        private async Task NewMatch(CancellationToken token)
        {
            SetControlsEnabled(false);

            Player black = new Player(comboBox1.SelectedIndex, 1);
            Player white = new Player(comboBox2.SelectedIndex, 2);
            double b = 0, w = 0;
            Tuple<int, int> move;
            label2.Text = "Black: ";
            label3.Text = "White: ";

            bool cancelled = false;
            for (int i = 0; i < (int)numericUpDown1.Value; i++)
            {
                board = new Board();
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        cancelled = true;
                        break;
                    }

                    move = await Task.Run(() => black.ChooseMove(board, (int)numericUpDown2.Value), token);
                    if (move != null)
                    {
                        await Task.Run(() => board.MakeMove(move.Item1, move.Item2, true), token);
                        DrawBoardState(board);
                        //Thread.Sleep(2000);
                    }
                    if (board.IsGameOver() == true)
                    {
                        UpdateScore(board, ref b, ref w);
                        break;
                    }

                    move = await Task.Run(() => white.ChooseMove(board, (int)numericUpDown2.Value), token);
                    if (move != null)
                    {
                        await Task.Run(() => board.MakeMove(move.Item1, move.Item2, false), token);
                        DrawBoardState(board);
                        //Thread.Sleep(2000);
                    }
                    if (board.IsGameOver() == true)
                    {
                        UpdateScore(board, ref b, ref w);
                        break;
                    }
                }
            }

            if (!cancelled)
            {
                if (numericUpDown1.Value == 1)
                {
                    Tuple<int, int> score = board.GetScore();
                    label2.Text = "Black: " + score.Item1.ToString();
                    label3.Text = "White: " + score.Item2.ToString();
                }
                else
                {
                    label2.Text = "Black: " + b.ToString();
                    label3.Text = "White: " + w.ToString();
                }
                SetControlsEnabled(true);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (tokenSource != null)
            {
                tokenSource.Cancel();
                tokenSource = null;
            }
            SetControlsEnabled(true);
        }

        private void SetControlsEnabled(bool enabled)
        {
            numericUpDown1.Enabled = enabled;
            numericUpDown2.Enabled = enabled;
            comboBox1.Enabled = enabled;
            comboBox2.Enabled = enabled;
            newMatchButton.Enabled = enabled;
        }
        

        //private bool DrawValidMoves() // zwraca czy sa jakies mozliwe ruchy
        //{
        //    bool result = false;
        //    Graphics g = pictureBox1.CreateGraphics();
        //    SolidBrush sb = new SolidBrush(Color.Blue);
        //    for (int i = 0; i < 8; i++)
        //    {
        //        for (int j = 0; j < 8; j++)
        //        {
        //            if(IsMoveValid(i,j) == true)
        //            {
        //                g.FillRectangle(sb,i * 50, j * 50, 50, 50);
        //                result = true;
        //            }
        //        }
        //    }
        //    return result;
        //}


    }
}
