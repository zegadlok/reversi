﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class HeuristicUntakeablePiecesScore : IHeuristic
    {
        private bool[,] processed = new bool[8, 8];

        private int AddScoreForUntakeablePiece(int cornerColor)
        {
            if (cornerColor == 1) return 8;
            else return -8;
        }

        private int CalculateScoreForUntakeableInnerPieces(Board board)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++) processed[i, j] = false;
            }

            int score = 0;
            int cornerColor = board.GetSquare(0, 0);
            bool check;
            int sum = 2;
            if (cornerColor != 0 && board.GetSquare(1, 0) == cornerColor && board.GetSquare(0, 1) == cornerColor)
            //upper left corner and second diagonal are filled
            {
                do //checking next diagonals
                {
                    check = false;                   
                    int i = sum - 1;
                    int j = 1;
                    if (board.GetSquare(sum, 0) == cornerColor) // from left border
                    {
                        while (board.GetSquare(i, j) == cornerColor && i > 0)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            --i;
                            ++j;
                        }
                        if (j == sum && board.GetSquare(0, sum) == cornerColor) check = true;
                    }

                    i = 1;
                    j = sum - 1;
                    if (board.GetSquare(0, sum) == cornerColor && check == false) // from upper border
                    {
                        while (board.GetSquare(i, j) == cornerColor && j > 0)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            ++i;
                            --j;
                        }
                    }
                    ++sum;
                } while (check == true && sum <= 7);
            }

            cornerColor = board.GetSquare(7, 7);
            sum = 12;
            if (cornerColor != 0 && board.GetSquare(6, 7) == cornerColor && board.GetSquare(7, 6) == cornerColor)
            //bottom right corner and second diagonal are filled
            {
                do //checking next diagonals
                {
                    check = false;
                    int i = sum-6;
                    int j =6;
                    if (board.GetSquare(sum-7, 7) == cornerColor) // from right border
                    {
                        while (board.GetSquare(i, j) == cornerColor && i < 7)
                        {
                            if(processed[i,j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            ++i;
                            --j;
                        }
                        if (i == 7 && board.GetSquare(7, sum - 7) == cornerColor) check = true;
                    }

                    i = 6;
                    j = sum - 6;
                    if (board.GetSquare(7,sum-7) == cornerColor && check == false) // from bottom border
                    {
                        while (board.GetSquare(i, j) == cornerColor && j < 7)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            --i;
                            ++j;
                        }
                    }
                    --sum;
                } while (check == true && sum > 7);
            }

            cornerColor = board.GetSquare(0, 7);
            int diff = 5;
            if (cornerColor != 0 && board.GetSquare(0, 6) == cornerColor && board.GetSquare(1,7) == cornerColor)
            //upper right corner and second diagonal are filled
            {
                do //checking next diagonals
                {
                    check = false;
                    int i = 7 - diff - 1;
                    int j = 6;
                    if (board.GetSquare(7 - diff, 7) == cornerColor) // from right border
                    {
                        while (board.GetSquare(i, j) == cornerColor && i > 0)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            --i;
                            --j;
                        }
                        if (i == 0 && board.GetSquare(0, diff) == cornerColor) check = true;
                    }

                    i = 1;
                    j = diff + 1;
                    if (board.GetSquare(0, diff) == cornerColor && check == false) // from upper border
                    {
                        while (board.GetSquare(i, j) == cornerColor && j < 7)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            ++i;
                            ++j;
                        }
                    }
                    --diff;
                } while (check == true && diff >= 0);
            }

            cornerColor = board.GetSquare(7,0);
            diff = -5;
            if (cornerColor != 0 && board.GetSquare(6,0) == cornerColor && board.GetSquare(7,1) == cornerColor)
            //bottom left corner and second diagonal are filled
            {
                do //checking next diagonals
                {
                    check = false;
                    int i = -diff + 1 ;
                    int j = 1;
                    if (board.GetSquare(-diff, 0) == cornerColor) // from left border
                    {
                        while (board.GetSquare(i, j) == cornerColor && i < 7)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            ++i;
                            ++j;
                        }
                        if (i == 7 && board.GetSquare(7, diff + 7) == cornerColor) check = true;
                    }

                    i = 6;
                    j = diff + 6;
                    if (board.GetSquare(7, diff + 7) == cornerColor && check == false) // from bottom border
                    {
                        while (board.GetSquare(i, j) == cornerColor && j > 0)
                        {
                            if (processed[i, j] == false)
                            {
                                processed[i, j] = true;
                                score += AddScoreForUntakeablePiece(cornerColor);
                            }
                            --i;
                            --j;
                        }
                    }
                    ++diff;
                } while (check == true && diff < 0);
            }

            return score;

        }
        private int CalculateScoreForUntakeableBorderPieces(Board board)
        {
            int score = 0;

            bool upperBorderFull = false;
            bool leftBorderFull = false;
            bool rightBorderFull = false;
            bool lowerBorderFull = false;

            int cornerColor = board.GetSquare(0, 0);
            if (cornerColor != 0) //check from upper left corner to right and down
            {
                score += AddScoreForUntakeablePiece(cornerColor);
                int i = 1;
                while (i < 7 && board.GetSquare(0, i) == cornerColor)
                {
                    score += AddScoreForUntakeablePiece(cornerColor);
                    ++i;
                }
                if (i == 7) upperBorderFull = true;

                i = 1;
                while (i < 7 && board.GetSquare(i, 0) == cornerColor)
                {
                    score += AddScoreForUntakeablePiece(cornerColor);
                    ++i;
                }
                if (i == 7) leftBorderFull = true;
            }

            cornerColor = board.GetSquare(7, 7);//check from bottom right corner to left and up
            if (cornerColor != 0)
            {
                score += AddScoreForUntakeablePiece(cornerColor);
                int i = 6;
                while (i >= 1 && board.GetSquare(7, i) == cornerColor)
                {
                    score += AddScoreForUntakeablePiece(cornerColor);
                    --i;
                }
                if (i == 0) lowerBorderFull = true;

                i = 6;
                while (i >= 1 && board.GetSquare(i, 7) == cornerColor)
                {
                    score += AddScoreForUntakeablePiece(cornerColor);
                    --i;
                }
                if (i == 0) rightBorderFull = true;
            }

            cornerColor = board.GetSquare(0, 7); //check from upper right corner to left and down
            if (cornerColor != 0)
            {
                score += AddScoreForUntakeablePiece(cornerColor);

                if (upperBorderFull == false) // border can already be full and checked
                {
                    int i = 6;
                    while (board.GetSquare(0, i) == cornerColor)
                    {
                        score += AddScoreForUntakeablePiece(cornerColor);
                        --i;
                    }
                }
                if (rightBorderFull == false) // border can already be full and checked
                {
                    int i = 1;
                    while (board.GetSquare(i, 7) == cornerColor)
                    {
                        score += AddScoreForUntakeablePiece(cornerColor);
                        ++i;
                    }
                }
            }

            cornerColor = board.GetSquare(7, 0);//check from bottom left corner to right and up
            if (cornerColor != 0)
            {
                score += AddScoreForUntakeablePiece(cornerColor);

                if (lowerBorderFull == false) // border can already be full and checked
                {
                    int i = 1;
                    while (board.GetSquare(7, i) == cornerColor)
                    {
                        score += AddScoreForUntakeablePiece(cornerColor);
                        ++i;
                    }
                }
                if (leftBorderFull == false)// border can already be full and checked
                {
                    int i = 6;
                    while (board.GetSquare(i, 0) == cornerColor)
                    {
                        score += AddScoreForUntakeablePiece(cornerColor);
                        --i;
                    }
                }
            }
            return score;
        }
        public int CalculateScore(Board board)
        {
            int score = 0;
            score += CalculateScoreForUntakeableBorderPieces(board);
            score += CalculateScoreForUntakeableInnerPieces(board);
            return score;
        }
    }
}
