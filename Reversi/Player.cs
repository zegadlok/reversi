﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Player
    {
        IHeuristic heuristic;
        int color;
        public Player(int h,int c)
        {
            color = c;
            switch(h)
            {
                case 0:
                    heuristic = new Heuristic1();
                    break;
                case 1:
                    heuristic = new Heuristic2();
                    break;
                case 2:
                    heuristic = new Heuristic3();
                    break;
                case 3:
                    heuristic = new Heuristic4();
                    break;
            }
        }

        public Tuple<int, int> ChooseMove(Board b, int depth)
        {
            List<Tuple<int, int>> legalMoves = GetLegalMoves(b, color);
            Node root = new Node(b);
            AlphaBeta(root, depth, int.MinValue, int.MaxValue, color == 1);
            Random r = new Random(DateTime.Now.Millisecond);
            List<Tuple<int, int>> chosenMoves = new List<Tuple<int, int>>();

            for(int i=0;i<legalMoves.Count;i++)
            {
                if (root.children[i].value == root.value) chosenMoves.Add(legalMoves[i]);
            }

            if (chosenMoves.Count == 0) return null;
            return chosenMoves[r.Next() % chosenMoves.Count];
        }

        public int AlphaBeta(Node root, int depth, int alpha, int beta, bool max)
        {
            int oppositeColor = color == 1 ? 2 : 1;
            int value;
            if (depth == 0)
            {
                int v = heuristic.CalculateScore(root.board);
                root.value = v;
                return v;
            }
            List<Tuple<int, int>> legalMoves = GetLegalMoves(root.board, max == true ? 1:2);
            if(legalMoves.Count == 0)
            {
                int v = heuristic.CalculateScore(root.board);
                root.value = v;
                return v;
            }

            foreach (Tuple<int, int> lm in legalMoves)
            {
                Board copy = root.board.CopyBoard();
                copy.MakeMove(lm.Item1, lm.Item2, color == 1);
                Node n = new Node(copy);
                root.AddChild(n);              
            }

            if (max==true)
            {
                value = int.MinValue;
                foreach(Node n in root.children)
                {
                    value = Math.Max(value, AlphaBeta(n, depth-1, alpha, beta, false));                    
                    alpha = Math.Max(alpha, value);                    
                    if (alpha > beta) break;                   
                }
                root.value = value;
                return value;
            }
            else
            {
                value = int.MaxValue;
                foreach (Node n in root.children)
                {
                    value = Math.Min(value, AlphaBeta(n, depth-1, alpha, beta, true));
                    beta = Math.Min(beta, value);
                    if (alpha > beta) break;
                }
                root.value = value;
                return value;
            }
        }

        private List<Tuple<int, int>> GetLegalMoves(Board board, int color)
        {
            List<Tuple<int, int>> legalMoves = new List<Tuple<int, int>>();

            for(int i=0;i<8;i++)
            {
                for(int j=0;j<8;j++)
                {
                    if (board.IsMoveValid(i, j, color == 1)) legalMoves.Add(new Tuple<int, int>(i, j));
                }
            }
            return legalMoves;
        }
    }
}
