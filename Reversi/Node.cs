﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Node
    {
        public Board board;
        public int value;
        public List<Node> children;

        public Node(Board b)
        {
            board = b;
            value = int.MinValue;
            children = null;
        }

        public void AddChild(Node n)
        {
            if (children == null) children = new List<Node>();
            children.Add(n);
        }
    }
}
