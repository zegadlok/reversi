﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Heuristic4 : IHeuristic
    {
        private Heuristic1 baseHeuristic = new Heuristic1();
        private HeuristicUntakeablePiecesScore hups = new HeuristicUntakeablePiecesScore();
        private bool[,] processed = new bool[8, 8];

        public int CalculateScore(Board board)
        {
            int score = baseHeuristic.CalculateScore(board);
            score += hups.CalculateScore(board);
            return score;
        }
    }
}
