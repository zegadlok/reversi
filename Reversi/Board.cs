﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Board
    {
        private int[,] boardState; //0-nic, 1-czarne, 2-biale, 
        //zamiast x i y jest indeksacja jak do macierzy, wiersze x kolumny
        private int black, white;

        public Board()
        {
            boardState = new int[8, 8];
            boardState[3, 3] = 2;
            boardState[4, 4] = 2;
            boardState[3, 4] = 1;
            boardState[4, 3] = 1;
            black = white = 2;
        }

        public int GetBlack() { return black; }
        public int GetWhite() { return white; }
        public Tuple<int, int> GetScore() { return new Tuple<int, int>(black, white); }

        public bool DoesBlackHaveMoves()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (IsMoveValid(i, j, true) == true) return true;
                }
            }
            return false;
        }

        public bool DoesWhiteHaveMoves()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (IsMoveValid(i, j, false) == true) return true;
                }
            }
            return false;
        }

        public bool IsGameOver()
        {
            if (black + white == 64) return true;
            if (black == 0 || white == 0) return true;
            if (DoesBlackHaveMoves() == false && DoesWhiteHaveMoves() == false) return true;

            return false;
        }

        public int GetSquare(int x, int y) { return boardState[x, y]; }

        public void ChangeSquare(int x, int y, int color) { boardState[x, y] = color; }

        private void UpdateScore(int color)
        {
            if(color == 1)
            {
                ++black;
                --white;
            }
            else
            {
                --black;
                ++white;
            }
        }

        public void MakeMove(int x, int y, bool blacksTurn)
        {
            int oppositeColor = blacksTurn ? 2:1;
            int color = blacksTurn ? 1:2;
            bool change = false;
            if (CheckRight(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x + 1;
                while (boardState[i, y] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, y] = color;
                    ++i;
                }
            }
            if (CheckLeft(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x - 1;
                while (boardState[i, y] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, y] = color;
                    --i;
                }
            }
            if (CheckUp(x, y, blacksTurn) == true)
            {
                change = true;
                int i = y - 1;
                while (boardState[x, i] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[x, i] = color;
                    --i;
                }
            }
            if (CheckDown(x, y, blacksTurn) == true)
            {
                change = true;
                int i = y + 1;
                while (boardState[x, i] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[x, i] = color;
                    ++i;
                }
            }
            if (CheckDownRight(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x + 1;
                int j = y + 1;
                while (boardState[i, j] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, j] = color;
                    ++i;
                    ++j;
                }
            }
            if (CheckDownLeft(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x - 1;
                int j = y + 1;
                while (boardState[i, j] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, j] = color;
                    --i;
                    ++j;
                }
            }
            if (CheckUpRight(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x + 1;
                int j = y - 1;
                while (boardState[i, j] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, j] = color;
                    ++i;
                    --j;
                }
            }
            if (CheckUpLeft(x, y, blacksTurn) == true)
            {
                change = true;
                int i = x - 1;
                int j = y - 1;
                while (boardState[i, j] == oppositeColor)
                {
                    UpdateScore(color);
                    boardState[i, j] = color;
                    --i;
                    --j;
                }
            }
            if (change == true)
            {
                boardState[x, y] = color;
                if (color == 1) ++black;
                else ++white;
            }
        }

        public Board CopyBoard()
        {
            Board copy = new Board();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    copy.ChangeSquare(i, j, boardState[i,j]);
                }
            }
            return copy;
        }        

        public bool IsMoveValid(int x, int y, bool blacksTurn)
        {
            if (CheckRight(x, y, blacksTurn) == true) return true;
            if (CheckLeft(x, y, blacksTurn) == true) return true;
            if (CheckUp(x, y, blacksTurn) == true) return true;
            if (CheckDown(x, y, blacksTurn) == true) return true;

            if (CheckUpRight(x, y, blacksTurn) == true) return true;
            if (CheckUpLeft(x, y, blacksTurn) == true) return true;
            if (CheckDownRight(x, y, blacksTurn) == true) return true;
            if (CheckDownLeft(x, y, blacksTurn) == true) return true;

            return false;
        }

        private bool CheckRight(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 7) return false;
            int oppositeColor = blacksTurn ? 2:1;
            int color = blacksTurn ? 1:2;
            if (boardState[x + 1, y] != oppositeColor) return false;

            int i = x + 2;
            while (i < 8 && boardState[i, y] == oppositeColor)
            {
                ++i;
            }

            if (i == 8 || boardState[i, y] != color) return false;
            return true;
        }
        private bool CheckLeft(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 0) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x - 1, y] != oppositeColor) return false;

            int i = x - 2;
            while (i >= 0 && boardState[i, y] == oppositeColor)
            {
                --i;
            }

            if (i < 0 || boardState[i, y] != color) return false;
            return true;
        }

        private bool CheckUp(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || y == 0) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x, y - 1] != oppositeColor) return false;

            int i = y - 2;
            while (i >= 0 && boardState[x, i] == oppositeColor)
            {
                --i;
            }

            if (i < 0 || boardState[x, i] != color) return false;
            return true;
        }

        private bool CheckDown(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || y == 7) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x, y + 1] != oppositeColor) return false;

            int i = y + 2;
            while (i < 8 && boardState[x, i] == oppositeColor)
            {
                ++i;
            }

            if (i == 8 || boardState[x, i] != color) return false;
            return true;
        }

        private bool CheckDownRight(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 7 || y == 7) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x + 1, y + 1] != oppositeColor) return false;

            int i = x + 2;
            int j = y + 2;
            while (i < 8 && j < 8 && boardState[i, j] == oppositeColor)
            {
                ++i;
                ++j;
            }

            if (i == 8 || j == 8 || boardState[i, j] != color) return false;
            return true;
        }

        private bool CheckDownLeft(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 0 || y == 7) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x - 1, y + 1] != oppositeColor) return false;

            int i = x - 2;
            int j = y + 2;
            while (i >= 0 && j < 8 && boardState[i, j] == oppositeColor)
            {
                --i;
                ++j;
            }

            if (i < 0 || j == 8 || boardState[i, j] != color) return false;
            return true;
        }

        private bool CheckUpRight(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 7 || y == 0) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x + 1, y - 1] != oppositeColor) return false;

            int i = x + 2;
            int j = y - 2;
            while (i < 8 && j >= 0 && boardState[i, j] == oppositeColor)
            {
                ++i;
                --j;
            }

            if (i == 8 || j < 0 || boardState[i, j] != color) return false;
            return true;
        }

        private bool CheckUpLeft(int x, int y, bool blacksTurn)
        {
            if (boardState[x, y] != 0 || x == 0 || y == 0) return false;
            int oppositeColor = blacksTurn ? 2 : 1;
            int color = blacksTurn ? 1 : 2;
            if (boardState[x - 1, y - 1] != oppositeColor) return false;

            int i = x - 2;
            int j = y - 2;
            while (i >= 0 && j >= 0 && boardState[i, j] == oppositeColor)
            {
                --i;
                --j;
            }

            if (i < 0 || j < 0 || boardState[i, j] != color) return false;
            return true;
        }
    }
}
