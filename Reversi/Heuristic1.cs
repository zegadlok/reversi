﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Heuristic1 : IHeuristic
    {
        public int CalculateScore(Board board)
        {
            int score = 0;
            for(int i=0;i<8;i++)
            {
                for(int j=0;j<8;j++)
                {
                    if (board.GetSquare(i, j) == 0) continue;
                    if (board.GetSquare(i, j) == 1) ++score;
                    else --score;
                }
            }
            return score;
        }
    }
}
