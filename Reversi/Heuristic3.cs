﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public class Heuristic3 : IHeuristic
    {
        private Heuristic2 baseHeuristic = new Heuristic2();
        private HeuristicUntakeablePiecesScore hups = new HeuristicUntakeablePiecesScore();

        public int CalculateScore(Board board)
        {
            int score = baseHeuristic.CalculateScore(board);
            score += (hups.CalculateScore(board) * 4);
            return score;
        }
    }
}
