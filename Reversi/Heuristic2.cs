﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    class Heuristic2 : IHeuristic
    {
        private static int[,] scores = new int[,]
        { { 8,8,8,8,8,8,8,8}, { 8,4,4,4,4,4,4,8},
           { 8,4,2,2,2,2,4,8}, { 8,4,2,1,1,2,4,8},
           { 8,4,2,1,1,2,4,8}, { 8,4,2,2,2,2,4,8},
           { 8,4,4,4,4,4,4,8},{ 8,8,8,8,8,8,8,8}
        };

        public int CalculateScore(Board board)
        {
            int score = 0;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (board.GetSquare(i, j) == 0) continue;
                    if (board.GetSquare(i, j) == 1) score = score + scores[i,j];
                    else score = score - scores[i, j];
                }
            }
            return score;
        }
    }
}
