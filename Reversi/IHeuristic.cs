﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public interface IHeuristic
    {
        int CalculateScore(Board board); //1-czarne, 2-biale
    }
}
